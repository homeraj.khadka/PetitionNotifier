﻿using System;

namespace PetitionNotifier.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static string GetFormattedDate(this DateTime dateTime)
        {
            return $"[{dateTime.ToShortTimeString()}] ";
        }
    }
}