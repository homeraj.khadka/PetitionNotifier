﻿namespace PetitionNotifier.Core.Models
{
    public class PetitionInformation
    {                  
        public string Title { get; }

        public string Description { get; }

        public int SignatureCount { get; }

        public PetitionInformation(string title, string description, int signatureCount)
        {
            Title = title;
            Description = description;
            SignatureCount = signatureCount;
        }          
    }
}