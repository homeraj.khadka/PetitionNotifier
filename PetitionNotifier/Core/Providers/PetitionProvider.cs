﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using PetitionNotifier.Core.Models;

namespace PetitionNotifier.Core.Providers
{
    public class PetitionProvider : IDisposable
    {
        private static readonly Uri PetitionBaseUri;

        private readonly HttpClient _client;
        private readonly Timer _checkTimer;

        private string _title;
        private string _description;
        private int _previousSignatures;

        public delegate void OnSignatureCountChangeHandler(int count);
        public event OnSignatureCountChangeHandler OnSignatureCountChange;

        public int PetitionId { get; set; }

        static PetitionProvider()
        {
            PetitionBaseUri = new Uri("https://petition.parliament.uk/petitions/");
        }

        public PetitionProvider(int petitonId, double interval = 60 * 1000 /* every minute */)
        {
            PetitionId = petitonId;

            _client = new HttpClient { BaseAddress = PetitionBaseUri };

            _checkTimer = new Timer { Interval = interval };
            _checkTimer.Elapsed += CheckTimerElapsed;
        }

        private async void CheckTimerElapsed(object sender, ElapsedEventArgs e)
        {
            var currentSignatures = await GetCurrentSignatureCount();
            if (_previousSignatures >= currentSignatures)
                return;

            _previousSignatures = currentSignatures;
            OnSignatureCountChange?.Invoke(currentSignatures);
        }

        public void Start()
        {
            _checkTimer.Start();
        }

        public void Stop()
        {
            _checkTimer.Start();
        }
        
        public async Task<string> GetTitle(string source = "")
        {
            if (!string.IsNullOrWhiteSpace(_title))
                return _title;

            if (string.IsNullOrEmpty(source))
                source = await GetPetitionPageSource();

            var title = GetBetween(source, "<title>", " - Petitions</title>"); 
            return (_title = title);
        }

        public async Task<string> GetDescription(string source = "")
        {
            if (!string.IsNullOrWhiteSpace(_description))
                return _description;

            if (string.IsNullOrEmpty(source))
                source = await GetPetitionPageSource();
            
            var description = GetBetween(source, "og:description\" content=\"", "\" />");
            return (_description = description);
        }

        public async Task<int> GetCurrentSignatureCount(string source = "")
        {
            if (string.IsNullOrEmpty(source))
                source = await GetPetitionPageSource();
                                                                                                                                      
            var signatures = GetBetween(source, "count-number\">", " <span>");  
            return int.Parse(signatures, NumberStyles.AllowThousands);
        }

        public async Task<PetitionInformation> GetPetitionInformation()
        {
            if (string.IsNullOrWhiteSpace(_title) || string.IsNullOrWhiteSpace(_description))
            {
                var source = await GetPetitionPageSource();

                _title = await GetTitle(source);
                _description = await GetDescription(source);
                _previousSignatures = await GetCurrentSignatureCount(source);
            }

            return new PetitionInformation(_title, _description, _previousSignatures);
        }

        private async Task<string> GetPetitionPageSource()
        {                                                                          
            return await _client.GetStringAsync(PetitionId.ToString());
        }

        private static string GetBetween(string source, string begin, string end)
        {
            var start = source.IndexOf(begin, StringComparison.Ordinal) + begin.Length;
            var last = source.IndexOf(end, start, StringComparison.Ordinal) - start;

            return source.Substring(start, last);
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}