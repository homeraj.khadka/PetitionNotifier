﻿namespace PetitionNotifier.UI.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                _provider.Dispose();
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startButton = new System.Windows.Forms.Button();
            this.logRichTextBox = new System.Windows.Forms.RichTextBox();
            this.petitionDescriptionRichTextBox = new System.Windows.Forms.RichTextBox();
            this.petitionTitleTextBox = new PetitionNotifier.UI.Controls.PrefixTextBox();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Location = new System.Drawing.Point(324, 274);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(102, 26);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start..";
            this.startButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // logRichTextBox
            // 
            this.logRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logRichTextBox.Location = new System.Drawing.Point(13, 100);
            this.logRichTextBox.Name = "logRichTextBox";
            this.logRichTextBox.Size = new System.Drawing.Size(413, 168);
            this.logRichTextBox.TabIndex = 2;
            this.logRichTextBox.Text = "";
            // 
            // petitionDescriptionRichTextBox
            // 
            this.petitionDescriptionRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.petitionDescriptionRichTextBox.Location = new System.Drawing.Point(12, 34);
            this.petitionDescriptionRichTextBox.Name = "petitionDescriptionRichTextBox";
            this.petitionDescriptionRichTextBox.Size = new System.Drawing.Size(413, 60);
            this.petitionDescriptionRichTextBox.TabIndex = 2;
            this.petitionDescriptionRichTextBox.Text = "Loading petition..";
            this.petitionDescriptionRichTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.petitionDescriptionRichTextBox_KeyPress);
            // 
            // petitionTitleTextBox
            // 
            this.petitionTitleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.petitionTitleTextBox.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.petitionTitleTextBox.Location = new System.Drawing.Point(12, 12);
            this.petitionTitleTextBox.Name = "petitionTitleTextBox";
            this.petitionTitleTextBox.Prefix = "Loading petition..";
            this.petitionTitleTextBox.RealText = null;
            this.petitionTitleTextBox.Size = new System.Drawing.Size(414, 16);
            this.petitionTitleTextBox.TabIndex = 3;
            this.petitionTitleTextBox.Text = "Loading petition..";
            this.petitionTitleTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.petitionDescriptionRichTextBox_KeyPress);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(438, 310);
            this.Controls.Add(this.petitionTitleTextBox);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.petitionDescriptionRichTextBox);
            this.Controls.Add(this.logRichTextBox);
            this.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.RichTextBox logRichTextBox;
        private Controls.PrefixTextBox petitionTitleTextBox;
        private System.Windows.Forms.RichTextBox petitionDescriptionRichTextBox;
    }
}