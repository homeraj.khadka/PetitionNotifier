﻿using System;
using System.Drawing;
using System.Windows.Forms;
using PetitionNotifier.Core.Extensions;
using PetitionNotifier.Core.Providers;
using PetitionNotifier.UI.Extensions;

namespace PetitionNotifier.UI.Forms
{
    public partial class MainForm : Form
    {
        private readonly PetitionProvider _provider;

        public MainForm()
        {
            InitializeComponent();

            _provider = new PetitionProvider(104349);
            _provider.OnSignatureCountChange += OnOnSignatureCountChange;
        }

        private void OnOnSignatureCountChange(int count)
        {
            petitionTitleTextBox.RealText = $" ({count})";
            
            logRichTextBox.AppendText(DateTime.Now.GetFormattedDate(), SystemColors.Highlight);
            logRichTextBox.AppendText(@"Petition signature changed: ");
            logRichTextBox.AppendText(count.ToString(), Color.Green);
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            var combo = await _provider.GetTitleDescriptionSignatureCount();
            
            petitionTitleTextBox.Prefix = combo.SignatureCount.ToString();
            petitionTitleTextBox.RealText = $" ({combo.Title})";
            petitionDescriptionRichTextBox.Text = combo.Description;

            logRichTextBox.AppendText(DateTime.Now.GetFormattedDate(), SystemColors.Highlight);
            logRichTextBox.AppendText(@"Retrieved title and description.");
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (startButton.Text == @"Start..")
            {
                _provider.Start();
                startButton.Text = @"Stop..";

                logRichTextBox.AppendText(DateTime.Now.GetFormattedDate(), SystemColors.Highlight);
                logRichTextBox.AppendText(@"Started.");
            }
            else
            {
                _provider.Stop();
                startButton.Text = @"Start..";

                logRichTextBox.AppendText(DateTime.Now.GetFormattedDate(), SystemColors.Highlight);
                logRichTextBox.AppendText(@"Stopped.");
            }
        }

        private void petitionDescriptionRichTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}